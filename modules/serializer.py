#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# credit for this module owed to Mark Pilgrim's excellent volume Dive Into Python3
# Worth finding on Amazon https://www.amazon.com/gp/product/1430224150?ie=UTF8&tag=diveintomark-20&creativeASIN=1430224150
#

import pickle
import json
import time


def to_json(python_object):
    ''' '''
    if isinstance(python_object, time.struct_time):
        return {'__class__': 'time.asctime',
                '__value__': time.asctime(python_object)}
    if isinstance(python_object, bytes):
        return {'__class__': 'bytes',
                '__value__': list(python_object)}
    raise TypeError(repr(python_object) + ' is not JSON serializable')

def from_json(json_object):
    ''' '''
    if '__class__' in json_object:
        if json_object['__class__'] == 'time.asctime':
            return time.strptime(json_object['__value__'])
        if json_object['__class__'] == 'bytes':
            return bytes(json_object['__value__'])
    return json_object

def serial_save(fqfn, obj):
    ''' '''
    fqfn = f"{str(fqfn)+'.json'}"
    try:
        with open(fqfn, 'w', encoding='utf-8') as f:
            json.dump(obj, f, default=to_json)
        return fqfn
    except:
        return f'Failed to save.'

def serial_load(fqfn):
    ''' '''
    fqfn = f"{str(fqfn)+'.json'}"
    try:
        with open(fqfn, 'r', encoding='utf-8') as f:
            obj = json.load(f, object_hook=from_json)
        return obj
    except:
        return f'Failed to load.'

# TODO
def serial_del(fqfn):
    ''' '''
    fqfn = f"{str(fqfn)+'.json'}"
    try:
        # delete the file off the fs
        pass
    except:
        # oh, noes, the file doesn't exist
        # make sure you delete the key reference
        # dumps may happen intermittently with greater frequency @ memory thresholds
        pass
    
def pikkle_save():
    ''' '''
    # basis of an <<interface>>
    with open('jobs.pickle', 'wb') as f:
        pickle.dump(jobs, f)

def pikkle_load():
    ''' '''
    with open('jobs.pickle', 'rb') as f:
        entry2 = pickle.load(f)

def pikkle_delete():
    ''' '''
    

if __name__ == '__main__':

'''
TODO Link it up to something like class KeyStore() as a Jobber subclass or parent...
Still considering how this looks in terms of events and sequences within instances:
Should keys be instance related exclusively, what are the base cases, and for otherwise? 
'''
