#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
Created May 2018

@author: tim
http://mysite.com
https://www.tutorialspoint.com/python/python_sorting_algorithms.htm
'''


def bubble_sort(ls):
    """ Swap the elements to arrange in order. """
    for iter_num in range(len(ls)-1, 0, -1):
        for idx in range(iter_num):
            if ls[idx] > ls[idx + 1]:
                temp = ls[idx]
                ls[idx] = ls[idx + 1]
                ls[idx + 1] = temp
    return ls

# USAGE
# list = [19,2,31,45,6,11,121,27]
# bubblesort(list)
# print(list)


def merge_sort(ls):
    """ Merge sort the elements in a list """
    if len(ls) <= 1:
        return ls
# Find the middle point and devide it
    middle = len(ls) // 2
    left_list = ls[:middle]
    right_list = ls[middle:]

    left_list = merge_sort(left_list)
    right_list = merge_sort(right_list)
    return list(merge(left_list, right_list)) # returns the sorted list
# Merge the sorted halves
def merge(left_half,right_half):
    """ Merge supplied left and right halves helper to Merge Sort """
    res = []
    while len(left_half) != 0 and len(right_half) != 0:
        if left_half[0] < right_half[0]:
            res.append(left_half[0])
            left_half.remove(left_half[0])
        else:
            res.append(right_half[0])
            right_half.remove(right_half[0])
    if len(left_half) == 0:
        res = res + right_half
    else:
        res = res + left_half
    return res

# USAGE
# unsorted_list = [64, 34, 25, 12, 22, 11, 90]
# print(merge_sort(unsorted_list))


def insertion_sort(ls):
    """ Sort each element into a list upon iteration """
    for i in range(1, len(ls)):
        j = i - 1
        nxt_element = ls[i]
# Compare the current element with next one
        while (ls[j] > nxt_element) and (j >= 0):
            ls[j + 1] = ls[j]
            j = j - 1
        ls[j + 1] = nxt_element
    return ls

# USAGE
# list = [19,2,31,45,30,11,121,27]
# insertion_sort(list)
# print(list)


def shell_sort(ls):
    """ Shell sort a list """
    gap = len(ls) // 2
    while gap > 0:
        for i in range(gap, len(ls)):
            temp = ls[i]
            j = i
# Sort the sub list for this gap
            while j >= gap and ls[j - gap] > temp:
                ls[j] = ls[j - gap]
                j = j - gap
            ls[j] = temp
# Reduce the gap for the next element
        gap = gap // 2
    return ls

# list = [19,2,31,45,30,11,121,27]

# USAGE
# shellSort(list)
# print(list)


def selection_sort(ls):
    """ Selection sort a list """
    for idx in range(len(ls)):
        min_idx = idx
        for j in range(idx + 1, len(ls)):
            if ls[min_idx] > ls[j]:
                min_idx = j
# Swap the minimum value with the compared value
        ls[idx], ls[min_idx] = ls[min_idx], ls[idx]
    return ls

# USAGE
# l = [19,2,31,45,30,11,121,27]
# selection_sort(l)
# print(l)
