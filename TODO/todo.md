# TODO

Formulate a card data scheme which, in addition to attributes already incorporated, will provide properties to facilitate:
* in family heirarchical value
* family types of differing values
* types in families with differing values (by runtime methods and supplied rules in the datascheme)
    
Decide if a card data scheme also contains the ruleset framework or whether these are separate, or should be developed separately with the view they should be merged when other parts can better meet this objective
    
Look at factory class structure in the card refactor

Review possible use of super() and the MRO to facilitate variant objects from class chain inheritance
* Why doesn't an object instantiated with super() have the type class with name super.__MROchain__?

Spin out branches development and planning, pushing to master only when there is a significant update

Sublimate legacy_pushdown to development or other branch, as appropriate?