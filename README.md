# cardiddlio/dekz

Within cardio_choice.py is a proposed dictionary of decks to include front, back and meta matter lists, and other matter.

No classes have yet been created for hands, players, decks, games, rules, environment, network, and etc.

TODOs may eventually drive the shape of things and accelerate if there is some interest in pushing on a direction from critics and/or contributors.

Mostly, this is a learning exercise.

### Wishlist: 
- reinforced learning
- graphing player interactions and performance
- networking
- generating games from external rules and assets supplied by non-programmer game-makers

If you're inclined to add to this wish list, get in touch with rebelclause@gmail.com
