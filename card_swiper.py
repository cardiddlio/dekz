from random import choice

ranks = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'j', 'q', 'k', 'a']
bosses = ['goat', 'wolf', 'cougar', 'silverback']
suits = ["heart's", "spade's", "diamond's", "club's"]

# generate a deck
deck = []
for s in suits:
	for r in ranks:
		# deck.append(f'{r} of {s.capitalize()}')
		for b in bosses:
			deck.append(f'{r} of {s.capitalize() {b.capitalize()}}')
# report length of deck
print(f'Deck contains {len(deck)} cards.')

# some operations on deck
deck = [x.strip(r"'") for x in deck]
# convert the deck to a tuple of values
deck = tuple(deck)
print('deck type: ',type(deck))

# generators (convertable to lists comprehensions)
# convert generators to lists by changing the containing parenthesis on card_num and num to [] 

# both one() & other() work when either starts & card_num <list> and num <generator>
# only one() || other works when either starts & card_num <generator> && num <generator> 

# card_num = [x for x in range(0, len(deck))]
card_num = (x for x in range(0, len(deck)))
print('card_num type: ',type(card_num))

# num = [x for x in range(0x000, 0xFFFFF, len(deck))]
num = (x for x in range(0x000, 0xFFFFF, len(deck)))
print('num type: ',type(num))

def one():
	baggie = zip(num, card_num, deck)
	baggie = list(baggie)
	print(f'Zipped into a baggie:\n{baggie}')

def other():
	zipdict = {zip(card_num, num): zip(card_num, deck)}  # list(zip(card_num, num)) gives TypeError'
#	print(zipdict)
	for k in zipdict.keys():
		for v in zipdict[k]:
			v = list(v)
			p = v[:][1:] # select only the card value the pair
			# p = [_.lower() for _ in p]
			for _ in p:
				print(f':: {_.lower()} ::')

def choose():
	card = zip(num, card_num, deck)	
	card = list(card)
	print(choice(card))
	
""" 
play with generators and lists, as above
and switch around the order of:
one(),
other(),
and the choose() block,
as below
to see what happens

Q.
why are num, card_id, deck picked up when
passed to the zip function within discrete function scopes?
"""

other()

one()

try:
	choose()
except IndexError: #('Cannot choose from an empty sequence') from None
    print('Sorry, there are no cards from which to make a choice.')

