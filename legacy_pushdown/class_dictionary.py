#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import choice
from modules.dumpinspect import dump
from modules.decorpack import timer, logger

class Card(object): # TODO [] still a mess with methods not really working and missing
    """

    """
    chosen: int = 0
    visible: bool = True

    def __init__(self, indicies, rank, suit, value, isboss, chosen=0, visible=True):
        """ """
        self.indicies: int = indicies
        self.rank: str = rank
        self.suit: str = suit
        self.value: int = value
        self.isboss: bool = isboss
        self.chosen: int = chosen
        self.visible: bool = visible

    def __repr__(self):
        """ A meaningful description of this class. """
        return f'{self.__class__}[indicies={self.indicies}, rank=\'{self.rank}\', suit=\'{self.suit}\', value={self.value}, isboss={self.isboss}, chosen={self.chosen}, visible={self.visible}]'

    def suit(self, suit):
        self.suit: str = suit

    def rank(self, rank):
        self.rank: str = rank

    def value(self, value):
        self.value: int = value

    def isboss(self, isboss):
        self.isboss: bool = isboss

    def chosen(self, chosen):
        """ Instance counter """ #TODO [] Maybe this should work.
        self.chosen = self.chosen + chosen
#        self.chosen += 1

    def visible(self, visible):
        self.visible: bool = True

    def deboss(self): #method to deboss at request
        self.isboss()


def makecards():

    all = {'suit': ['Spades', 'Hearts', 'Diamonds', 'Clubs'], 'rank': [2, 3, 4, 5] , 'fils': [6, 7, 8, 9, 10], 'boss': ['Jack', 'Queen', 'King', 'Ace']}
    cards = []
    indicies: int = 1
    suit = all['suit']
    del all['suit']
    for s in suit:
        for r in all.keys():
            for v in all[r]:
                # ['indicies', 'rank', 'suit', 'value', 'isboss', 'chosen', 'visible']
                # check isboss
                chosen = 0
                visible = True
                if r == 'boss':
                    isboss = True
                else:
                    isboss = False
                # check value
                if isboss:
                    val = 10
                else:
                    value = int(v)
                cards.append(Card(indicies, str(v), s, value, isboss, chosen, visible))
                # send Ace to lower ranks within its class immediately following assignment
                if cards[-1].rank == 'Ace':
#                    print(cards[-1])
                    cards[-1].isboss = False
                    cards[-1].value = 1
#                print(cards[-1]) #made __repr__ work, comment it out in the class to see the difference
                indicies += 1
    all['suit'] = suit
    return cards


def play(cards):
    deck = []
    deck.clear()
    c = []
    c.clear()
    c.extend(cards)
#    print(c[10:-9]) #sample
    # reset visibility on all card instances
    for x in c:
        x.visible = True

    keypress = ""
    while len(c) > 0 and keypress != 'q':
        keypress = ""
        keypress = input('q to Quit || Any key and/or Enter to choose a card: ')
        if keypress != 'q':
            thiscard = choice(c)
#            print(thiscard)
            thiscard.chosen += 1
            # keep the card, but move it out of the deck
            deck.append(thiscard)
            c.remove(thiscard)
            mycard = f'{thiscard.rank} of {thiscard.suit}'
            print(f"Your card {mycard} chosen {thiscard.chosen} time(s)")
            # set the card to invisible
            thiscard.visible = False

    msg = f"""

  {len(deck)} cards chosen in order of selection, above.

    Exiting..."""[4:-2]

    for x in deck:
        print(f'{x.rank} of {x.suit} \t\tseen {x.chosen}.')
    print(msg)

    msg = f"""

  {len(c)} cards not chosen, above.

    Exiting..."""[4:-2]

    for x in c:
        print(f'{x.rank} of {x.suit} \t\tseen {x.chosen}.')
    print(msg)
    # Go Again
    keypress = ""
    while keypress != 'n':
        msg = r"""
        Go Again? 'n' quits
        """[:]
        keypress = input(msg)
        if keypress == 'n':
            print('See you again next time.')
            return
        else:
            play(cards)
            return

class Players(object): #dictionary of players?
    """
    """
    def __init__():
        """
        # INFO [
        'first_name': "",
        'last_name': "",
        'game_latest': [holds3],
        'game_game': [],
        'game_listof_played_ever': [list {dict keys of rank/interaction stats}],
         ]
        """

def deal():
    pass

@logger
def find(objlist=None, matchtype=None, operator=None, range=None, rank=None, suit=None, seen=None, jobname=None):
    # (objlist, matchtype, operator, range, rank, suit, seen, jobname)
    """ State change of game sequence or game environment.\n
    Save Check and return matches to conditions.\n
    Likely divvied to State Machine framework and/or to class methods.\n
    For now, each request is separate, and merely results in a pretty print.
    """
    # sort received args and assign others as per namespace safe data
#    print(find())
    cards = []
    cards.extend(objlist)
    print(cards) # fooled into thinking 'Eureka', but it's just the existing cards list... much to be done...
#    args = list(*args)

    def runit():
        """ Run an existing job after checking its validity. """
        print(r'Run the job.')

    def createit():
        """ Create a job if one matching the arguments received does not exist, then run it. """
        jobs[jobname] = [objlist, matchtype, operator, range, rank, suit, seen] #should have create date and modified date, possibly where jobname
        print(r"Create the job b/c it doesn't exist, then run it.")
        runit()

    def parse_operator():
        """ Parse the operator parameter. """
        pass

    # sort conditions: job exists?
    try:
        check = jobs[jobname]
    except KeyError:
        createit()

    if not jobs[jobname] == None: # already a job, so run it
        runit()
        # check to see if it has changed... eek
    else: # create the job, then runit
        if jobname == None:
            pass
        else:
            jobs[jobname] = []

# TODO []: Upgrade to a dictionary form for args.
# TODO []: Pickle and/or JSONify jobs.
# TODO []: Sketch out the State Machine and hook it in.
# TODO []: Refactor to Command class?, possibly implement event timer(s)
#
#    if objlist is None:
#        pass
#    else:
#        if matchtype == 'all':
#            for suit in cards:
#                if cards.seen == seen:
#                    print(suit)
#                else:
#                    pass
#        else:
#            pass

#    elif matchtype == 'one':
#
#    elif matchtype == 'any':


if __name__ == '__main__':
    global jobs
    jobs = {}
    cards = makecards()
#    print(cards)
    play(cards)
#    deal(cards)
    alist = []
    blist = []
    clist = []
    criteria = ('all', 'Queen', 'all')
    print('c2', criteria[1])
    for x in cards:
        if x.rank == criteria[1]:
            alist.append(x)
        else:
            pass
    print('The \'A\' list: ', alist, '\n\n')

    # test and return data when condition
    # (objlist, matchtype, operator, range, rank, suit, seen, jobname)
#   find(cards, matchtype, operator, range, rank, suit, seen, jobname)
    find(cards, 'all','greater', 2 , None ,'diamonds', None ,'job1')
# TODO [] Keep in mind the job queue will need to be rigorous but easy
# to understand in both design and runtime contexts
# so a designer can make the best use of conditions at all times the time is right
# =============================================================================
#     find(cards, 'one', , 'diamonds', 'king', 'job2')
#     find(cards, 'all', , 'greater', 1, 'job3')
#     find(cards, 'any', , 'queen', 'less', 3, 'job4')
#     find(cards, 'any', , 'spades', 'greater', 2, 'job5')
# =============================================================================
# TODO [x] Refactor __repr__... looks and reads better.
# TODO [] State Machine classes generalized flow
# TODO [] Hand tracking/dealing
# TODO [] Hand evaluation
# TODO [] Player sequence condition, bid initiation and round, response and/or generalized self_review/setup/reveal/exchange, etc.
# TODO [] Card class methods relating to setting value
# TODO [in progress] Find a card by a value in cards [], Create persistent jobs and test conditions. Hook it together.
# TODO [] Expand cards[] to contain multiple decks, each as a list, or perhaps, better, a dictionary or a namedtuple?
# TODO [] Save find conditions, run check as changes occur, kick off event/display/sequence
