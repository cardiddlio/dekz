#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

from dataclasses import make_dataclass
from random import choice


def makecards():
    card = make_dataclass('Card', ['indicies', 'rank', 'suit', 'value', 'isboss', 'chosen', 'visible'])
    all = {'suit': ['Spades', 'Hearts', 'Diamonds', 'Clubs'], 'rank': [2, 3, 4, 5, 6, 7, 8, 9, 10], 'boss': ['Jack', 'Queen', 'King', 'Ace']}
    cards = []
    indicies: int = 1
    suit = all['suit']
    del all['suit']
    for s in suit:
        for r in all.keys():
            for v in all[r]:
                # ['indicies', 'rank', 'suit', 'value', 'isboss', 'chosen', 'visible']
                # check isboss
                if r == 'boss':
                    bosser = True
                else:
                    bosser = False
                # check value
                if bosser:
                    val = 10
                else:
                    val = int(v)
                cards.append(card(indicies, str(v), s, val, bosser, 0, True))
#                print(cards[-1])
                indicies += 1
    all['suit'] = suit
    return cards


def play(cards):
    deck = []
    deck.clear()
    c = []
    c.clear()
    c.extend(cards)
    # reset visibility on all card instances
    for x in c:
        x.visible = True

    keypress = ""
    while len(c) > 0 and keypress != 'q':
        keypress = ""
        keypress = input('q to Quit || Any key and/or Enter to choose a card: ')
        if keypress != 'q':
            thiscard = choice(c)
#            print(thiscard)
            thiscard.chosen += 1
            # keep the card, but move it out of the deck
            deck.append(thiscard)
            c.remove(thiscard)
            mycard = f'{thiscard.rank} of {thiscard.suit}'
            print(f"Your card {mycard} chosen {thiscard.chosen} time(s)")
            # set the card to invisible
            thiscard.visible = False

    msg = f"""

  {len(deck)} cards chosen in order of selection, above.

    Exiting..."""[4:-2]

    for x in deck:
        print(f'{x.rank} of {x.suit} \t\tseen {x.chosen}.')
    print(msg)

    msg = f"""

  {len(c)} cards not chosen, above.

    Exiting..."""[4:-2]

    for x in c:
        print(f'{x.rank} of {x.suit} \t\tseen {x.chosen}.')
    print(msg)
    # Go Again
    keypress = ""
    while keypress != 'n':
        msg = r"""
        Go Again? 'n' quits
        """[:]
        keypress = input(msg)
        if keypress == 'n':
            print('See you again next time.')
            return
        else:
            play(cards)
            return


if __name__ == '__main__':
    cards = makecards()
    play(cards)
