#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 10 23:11:21 2018

@author: tim
"""
import sys
from random import choice


class Card():
    """
    Card
    """
# =============================================================================
#     name: str = f'Card Name'
#     indicies: int = 0
#     rank = ''
#     suit: str = ''
#     value: int = 2
#     isboss: bool = False
#     chosen: int = 0
#     visible: bool = True
# =============================================================================
    # internal running count on construction
    makecount: int = 0

    def __init__(self, dictionary):
        """ """
#        for k, v in dictionary.items():
#            setattr(self, k, v)
        self.__dict__.update(dictionary) # put all dictionary items on the class
        self.indicies = self._count()
        self.isboss = self.switchboss()
        self.value = self._value()
        self.name = self._name()
        self.visible = self.switchvisible('on')
        # make indicies as a count

    def switchvisible(self, switch):
        # criteria ?
        if switch == 'on':
            return True
        else:
            return False

    def switchboss(self, switch=None):
        """ """
        try:
            blue = int(self.rank)
            return False
        except ValueError:
            return True

    def choose(self):
#        print('in chosen', self.chosen)
        self.chosen += 1
#        self.chosen = chosen

    def _value(self): # internal
        if self.value == None:
            if self.isboss is True:
                if self.rank == 'Ace':
                    return 11
                else:
                    return 10
            else:
                if self.rank == 'Ace':
                    return 1
                else:
                    return self.rank
        else:
            pass

    def _count(self): # keeps count of all cards made in session
        Card.makecount += 1
        return Card.makecount

    def _name(self):
        return f'{self.rank} of {self.suit}'

# =============================================================================
#     def __str__(self):
#         """ """
#         pass
# =============================================================================

    def __repr__(self):
        """ """
        return f'indicies={self.indicies}, name={self.name}, value={self.value}, isboss={self.isboss}, visible={self.visible}, chosen={self.chosen}'

    def __iter__(self):
        """ """
        pass
        # def __next__(self): #for the other
        #    """ """
        #    pass

    def __get__(self):
        """ """
        pass

    def __set__(self):
        """ """
        pass

    def __del__(self):
        """ """
        pass

    @classmethod
    def __clsmethod__(self):
        """ """
        pass

    @staticmethod
    def __statmethod__(self):
        """ Function referenced in a class namespace as a normal function
        without accessing self. """
        pass


class Deck():
    """
    """
#    buff = []
#    cards = []
#    immutable = []
    # TODO [] Will abstracting further determine counter should move here as Deck attribute?
    suits = ['Men', 'Hobbits', 'Elves', 'Orcs']
    ranks = [2, 3, 4, 5, 6, 7, 8, 9, 10, 100, 'Trailer Park Super', 'Jack', 'Queen', 'King', 'Ace']

    def __init__(self):
        """ """
        cards = []
        buff = []
        immutable = []
        self.cards = cards
        self.buff = buff

        for s in Deck.suits:
            for r in Deck.ranks:
                self.cards.append(Card({'name': None,
                                   'indicies': None,
                                   'rank': r,
                                   'suit': s,
                                   'value': None,
                                   'isboss': None,
                                   'chosen': 0,
                                   'visible': None
                                   }))

        self.immutable = self.cards[:]


    def flip_all(self, pick):
        Card.choose(pick)
        self.buff.append(pick)
#        print(f'buff {len(self.buff)}')
        self.cards.remove(pick)
#        print(f'cards {len(self.cards)}')
        print(pick.name)

    def restore(self):
        for c in self.immutable:
            c.chosen = 0
            self.buff.clear()
            self.cards.clear()
        self.cards = self.immutable[:]

# =============================================================================
#     def __count__(self):
#         return f'{len(self)}'
# =============================================================================


def main():
    """ Describes function main() """
    print('sys.argv[:]:', vars)
    pass


if __name__ == "__main__":

    # vars=[]
    vars = sys.argv[:]
    main()

    a = Deck()
    print('a cards: ', a.cards[0:])

    while len(a.cards) > 0:
        a.flip_all(choice(a.cards[:]))
    a.restore()
    print('len buff', len(a.buff))

    for card in a.immutable:
        for x in range(200):
            Card.choose(card)

    print('cards first and last:\n',a.cards[0::len(a.immutable)-1])
    print('len cards', len(a.cards))
#    Deck.restore(Deck)
    a.restore()
    print('len immut', len(a.immutable))
    print('immut first and last:\n',a.immutable[0::len(a.immutable)-1])

    print(f'Class Card says [{Card.makecount}] cards were made.')

