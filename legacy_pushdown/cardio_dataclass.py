#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

from dataclasses import make_dataclass
from random import choice


def makecards():
    card = make_dataclass('Card', ['indicies', 'rank', 'suit', 'value', 'isboss', 'chosen', 'visible'])
    suit = ['Spades', 'Hearts', 'Diamonds', 'Clubs']
    rank = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King', 'Ace']
    deck = []
    cards = []
    indicies: int = 1
    for s in suit:
        for r in rank:
            if not type(r) == str:
                deck.append([indicies, str(r), s, r, False, 0, True])
                cards.append(card(indicies, str(r), s, r, False, 0, True))
            else:
                deck.append([indicies, str(r), s, 10, True, 0, True])
                cards.append(card(indicies, str(r), s, 10, True, 0, True))
            indicies += 1
    del indicies
    return cards


def play(cards):
    deck = []
    deck.clear()
    c = []
    c.clear()
    c.extend(cards)
    for x in c:
        x.visible = True

    keypress = ""
    while len(c) > 0 and keypress != 'q':
        keypress = ""
        keypress = input('q to Quit || Any key and/or Enter to choose a card: ')
        if keypress != 'q':
            thiscard = choice(c)
#            print(thiscard)
            thiscard.chosen += 1
            deck.append(thiscard)
            c.remove(thiscard)
            mycard = f'{thiscard.rank} of {thiscard.suit}'
            if thiscard.visible:
                print(f"Your card {mycard} chosen {thiscard.chosen} time(s) with visibility {thiscard.visible}.")
                thiscard.visible = False
            elif not thiscard.visible:
                print(f"Your card {mycard} chosen {thiscard.chosen} time(s) with visibility {thiscard.visible}.")
                thiscard.visible = False
            else:
                pass
    msg = f"""

  {len(deck)} cards chosen in order of selection, above.

    Exiting..."""[4:-2]

    for x in deck:
        print(f'{x.rank} of {x.suit} \t\tseen {x.chosen}.')
    print(msg)

    msg = f"""

  {len(c)} cards not chosen, above.

    Exiting..."""[4:-2]

    for x in c:
        print(f'{x.rank} of {x.suit} \t\tseen {x.chosen}.')
    print(msg)
    # Go Again
    keypress = ""
    while keypress != 'n':
        msg = r"""
        Go Again? 'n' quits
        """[:]
        keypress = input(msg)
        if keypress == 'n':
            print('See you again next time.')
            return
        else:
            play(cards)
            return


if __name__ == '__main__':
    cards = makecards()
    play(cards)
