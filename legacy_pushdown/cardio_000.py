#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thurs Jul  5 06:50:28 2018

@author: tim
"""

from random import choice, shuffle
# from future import dataclass

# TODO [] read in from CSV, JSON, TOML, YAML
# TODO [] pickle to <format> load and runtime dump in the above context
# TODO [] flask the app, then interface using CSS and Brython to handle DOM elements
# TODO [] idemtify class divisions, divvying unique and shared attributes and methods
# TODO [] re. class divvy: investigate python3.7 dataclass suitability; too, 'is each card a class instance?'; possible superclass with instance overrides on get, set, del, etc.
# TODO [] accommodate high/low value for same card, ie. rank and boss status depending on rules of play and during game play: KING Me method()


def main():
    """
    This is a temporary script file.
    """
    def makedeck(suits=None, ranks=None, bosses=None, values=None):
        """
        Probably going to become class Deck(object): pass
        """
        # identity attribs
        suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades']
        # TODO [] embedded levels?? ie. for each value parsing a list of lists, list of tuples or comma separated value in top-level list
        ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10']  # same as above?
        bosses = ['Jack', 'Queen', 'King', 'Ace']  # same as above?
        values = ()  # tuple of suit_, rank_ & face_ values lists, or generate
        indicies = [x for x in range(len(suits)*(len(bosses)+len(ranks)))]
        print(indicies)
        # TODO [x]: How about an intrinsic index[] zipped into the tuple with rank/value?
        # TODO [?]: High/low flag/value, maybe calculated or set (min, max, median)?
        # visual attribs
        face_front = []
        face_back = []
        face_meta = []

        if len(values) == 0:
            """ values handling/generation separate from card type construction
            as a precaution, possibly aiding future expansion """
            suit_values = []
            rank_values = []
            boss_values = []
            # other categories of cards??
            do_values = []  # linearly derived values, even assigned to suits,
            # TODO UX: ordering categories and their lists, deciding on value (de)increment assignment/expression
            y = 0
            for x in range(len(suits)):  # TODO []: ID lowest baseline card value and adjust counter
                suit_values.append(1)
                for x in range(len(ranks)):
                    do_values.append(x+1)
                    rank_values = do_values[:]
                    y = x
                for x in range(len(bosses)):
                    do_values.append(((x+1)*10)+y)
                    boss_values = do_values[-(len(bosses))]
            print(do_values)
        else:  # unpack
            suit_values = values[1]
            rank_values = values[2]
            boss_values = values[3]
            # other categories?

        # each card becomes a tuple of suit+rank, value, **matters
        deckpack = []
        deck = []
        for s in suits:
            # TODO [?] generalize to list of list categories; ranks, bosses, warriors, supervisors, gnomes ...
            for r in ranks:
                deckpack.append(f'{r} of {s}')
            for b in bosses:
                deckpack.append(f'{b} of {s}')
        deck = zip(indicies, deckpack, do_values)

        # unpack the iterable into the deck
        del deckpack
        deckpack = []
        try:
            while deck:
                deckpack.append(deck.__next__())
        except StopIteration:
            pass

        thisdeck = {
            "suits": suits,
            "ranks": ranks,
            "face_front": face_front,
            "face_back": face_back,
            "face_meta": face_meta,
            "suit_values": suit_values,
            "rank_values": rank_values,
            "boss_values": boss_values,
            "deck": deckpack
            }
        return thisdeck

    # choose at random and save to new array, removing from deck
    def players():
        players = 0
        while players < 1:
            players = 1
            try:
                players = int(input('Choose Number of Players: '))
            except:
                raise Exception('Invalid input. Please try again')
            finally:
                return players

    def randomize(thedeck):
        """ produce a function which randomizes a deck x number of times
        according to a lambda function closure (x)
        """
        randdeck = []
        decklist = thedeck  # does this make remove() on the original below?
        for c in range(len(thedeck)):
            selcard = choice(decklist)
            randdeck.append(selcard)
            decklist.remove(selcard)
#        thedeck = randdeck
        return f'Shuffled using randomize(): {thedeck}'

    print(f'Playing with {players()} players.')

    mydeck = makedeck()
#    print(mydeck)
    print(mydeck['deck'])
# =============================================================================
#    print(randomize(mydeck['deck']))
#     print(randdeck)
#     mydeck['deck'] = randdeck
#     print(mydeck['deck'])
# =============================================================================

    # list_tuples_example_for_lambda_sort = [(1, 'd'), (2, 'b'), (4, 'a'), (3, 'c')]
    # sort cards by value
    print(f"\nSorted deck: {sorted(mydeck['deck'], key=lambda x: x[0])}")
    print(f"\nShuffled deck: {shuffle(mydeck['deck'])}")  # apparently you get random through module random.choice()

    # well, I almost lied. Still need choice, but you can whittle through the original deck and replace it
    randdeck = []
    decklist = mydeck['deck']
    for c in range(len(mydeck['deck'])):
        selcard = choice(decklist)
        randdeck.append(selcard)
        decklist.remove(selcard)
    mydeck['deck'] = randdeck
    print('Shuffled using choice()', mydeck['deck'])

    keypress = ""
    while keypress != 'q':
        keypress = ""
        keypress = input('q to Quit || Any key and/or Enter to choose a card: ')
        if keypress != 'q':
            print(f"You pressed {keypress} and your card is the {choice(mydeck['deck'])}")
        # TODO remove the card chosen from the deck
        # TODO when all cards are gone show the ordered choices
        # TODO press any key to exit, or restart with each previous session choice list showing

        # make each card a callable in a named tuple?
        # vs. make each card an instance of a class, which class pattern: factory, emitter, prototype, etc.?


if __name__ == '__main__':

    main()
