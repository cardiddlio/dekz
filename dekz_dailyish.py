#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 10 23:11:21 2018

@author: tim
"""

import sys
from random import choice
#from sphinx.ext.todo import Todo
try:
    import modules.serializer as j_son
except:
    pass
import pickle
import time
# import json
import urwid


class Card():
    """
    Card
    """
# =============================================================================
#     name: str = f'Card Name'
#     indicies: int = 0
#     rank = ''
#     suit: str = ''
#     value: int = 2
#     isboss: bool = False
#     chosen: int = 0
#     visible: bool = True
# =============================================================================
    # internal running count on construction
    makecount: int = 0
    decklength: int = 0
    length: int = 0

    def __init__(self, dictionary):
        """ Some fields need to be calculated """
        # TODO [] determine whether the self.__dict__ could be a named dictionary and at what cost/benefit
#        for k, v in dictionary.items():
#            setattr(self, k, v)
        self.__dict__.update(dictionary) # put all dictionary items on the class
        self.indicies = self._count()
        self.isboss = self.switchboss()
        self.value = self._value() # already in class dictionary
        self.name = self._name() # needs to be calculated
        self.visible = self.switchvisible('off')
        self.holder = self.switchhold
        self.where = self.switchwhere()
        self.decknum = self.decknum


    def switchvisible(self, switch):
        # criteria ?
        if switch == 'on':
            return True
        else:
            return False

    def switchboss(self, switch=None):
        """ Red pill, or blue pill? """
        try:
            blue = int(self.rank)
            return False
        except ValueError:
            return True

    def switchhold(self, holder=None): # (whohas, whereis)
        # holder - is: dealer, player[#, or name], buffer
        try:
            self.holder = holder
        except:
            pass

    def switchwhere(self, displayelement=None): # (whohas, whereis)
        # gamedisplay ie. in: hand, playpot, deck
        # move card obj to the correct array or set the correct value reflective of new/changed status
        try:
            return displayelement
        except:
            pass

    def chg_property_x(self, x, xval):
        # does the property exist, it should if you're calling out to change it?
        # doubling down on the received parameters?
        self.__dict__[x] = xval # how to find it properly, only if self has a named dict

    @classmethod
    def choose(self):
#        print('in chosen', self.chosen)
        self.chosen += 1
#        self.chosen = chosen

    def _value(self): # internal
        if self.value == None:
            if self.isboss is True:
                if self.rank == 'Ace':
                    return 11
                else:
                    return 10
            else:
                if self.rank == 'Ace':
                    return 1
                else:
                    return self.rank
        else:
            pass

    def _count(self): # keeps count of all cards made in session
        Card.makecount += 1
        return Card.makecount


    def _name(self):
        return f'{self.rank} of {self.suit}'

# =============================================================================
#     def __str__(self):
#         """ """
#         pass
# =============================================================================

    def __repr__(self):
        """ """
        return f'[decknum="{self.decknum}", indicies="{self.indicies}", name="{self.name}", value="{self.value}", isboss="{self.isboss}", visible="{self.visible}", chosen="{self.chosen}", where="{self.where}", property_x="{self.property_x}", beats_rank="{self.beats_rank}", beats_suit="{self.beats_suit}", beat_by="{self.beat_by}", beats_all="{self.beats_all}", holder="{self.holder}"]\n'

    def __iter__(self):
        """ """
        pass
        # def __next__(self): #for the other
        #    """ """
        #    pass

    def __get__(self, x, y):
        """ """
        pass

    def __set__(self, x, y):
        """ """
        pass

    def __del__(self):
        """ """
        pass

    def __eq__(self, other):
        if other.__class__ is not self.__class__:
            return NotImplemented
        return (self.rank, self.suit) == (other.rank, other.suit)

    @classmethod
    def __clsmethod__(self):
        """ """
        pass

    @staticmethod
    def __statmethod__(self):
        """ Function referenced in a class namespace as a normal function
        without accessing self. """
        pass


class Deck():
    """
    """
    decklists = []
#    cards = []
#    buff = []
#    immutable = []
    length = 0
    # TODO [] Will abstracting further determine counter should move here as Deck attribute?
#    suits = ['Men', 'Hobbits', 'Elves', 'Orcs']
#    ranks = [2, 3, 4, 5, 6, 7, 8, 9, 10, 100]
#    bosses = ['Trailer Park Super', 'Jack', 'Queen', 'King', 'Ace']
#    numdecks = 1
    # TODO [?] create immutable() as a class variable array/list?

    def __init__(self, rulepack):
        """ """
        self.immutable = ()
        self.cards = []
        self.buff = []
        Deck.decklists.append(self.immutable)
        Deck.decklists.append(self.cards)
        Deck.decklists.append(self.buff)
        numdecks = rulepack.game['numdecks']
        while numdecks > 0:
            # TODO: should self.cards, self.buff, self.immutable be raised to class var?
            # decided to keep a class list of deck instances, but not the lists of cards intrinsic to all instances
            # the method as is should allow one card's reference to all other objects to be deleted
            # while the remaining cards retain the object reference... test TODO: does a card object get destroyed globally if deleted from a card instance cards list??
            for s in rulepack.game['suits']:
                for r in rulepack.game['ranks']:
                    if r == rulepack.game['ranks'][0]:
                        # is first card
                        first = True
                    else:
                        first = False
                    beats = self.beat_rules(first, r, s)
                    self._makecard(r, s, numdecks, beats)
                for r in rulepack.game['bosses']:
                    beats = self.beat_rules(first, r, s)
                    self._makecard(r, s, numdecks, beats)
                    # reserved for if/when a suit
            numdecks -= 1
        self.immutable = self.cards[:]
        self.immutable = tuple(self.immutable)
        beats = self.beat_rules(first, r, s) # last card
            # set Deck.deckinstances with name of calling
            # seen the deck instance before; does it qualify for inclusion?
        # should the instance be available at this point... may have got around it by putting one in app(), instantiating on the interpreter stack then deleting it, so it stuck, have to run fresh
        Deck.decklists.append((Card.makecount, self))
#        print('Deck Instances:', Deck.deckinstances)

    def _makecard(self, *args): # r, s, numdecks, beats):
        # make indicies as a count
#        print(beats)
#        print(args)
        self.cards.append(Card({'name': None,
                           'indicies': None,
                           'rank': args[0],
                           'suit': args[1],
                           'value': None,
                           'isboss': None,
                           'chosen': 0,
                           'visible': None,
                           'holder': ['deck', 'dealer', 'player', 'rubber', 'etc'],
                           'where': [],
                           'decknum': args[2],
                           'property_x': None,
                           'beats_rank': args[3][0],
                           'beats_suit': args[3][1],
                           'beat_by': args[3][2],
                           'beats_all': args[3][3]
                           }))
    def beat_rules(self, first, r, s): # TODO [] fawlty rulesets, lacks conductor of calling origin/purpose
        # TODO [] alternatively, obtain from Rulepack as an iterable
        _beats_rank = r
        _beats_suit = None
        _beat_by = 'next highest card'
        _beats_all = False

        if Card.makecount > 0:
#            print(Card.makecount)
            try:
                _beats_rank = self.cards[-1].rank
                _beats_suit = None
#                _beats_suit = self.cards[-1].suit #false rule
                _beat_by = 'next highest card any suit'
            except:
                pass
        else:
            pass

        if first:
            _beats_rank = r
        else:
            pass

        return [_beats_rank, _beats_suit, _beat_by, _beats_all]

    def _decklength(self): # keeps the last known length
        try:
            Deck.length = len(self.cards)
        except:
            Deck.length = 0 # TODO same thing set in init... getting there, or rip it out
        return length

    def flip_all(self):
        while len(self.cards) > 0:
            pick = choice(self.cards[:])
            Card.choose(pick)
            self.buff.append(pick)
            print(f'buff {len(self.buff)}')
            self.cards.remove(pick)
            print(f'cards {len(self.cards)}')
            print(f'{pick.name} worth {pick.value}')

    def restore(self):
        # TODO[] Determine 'levels' of reset, and/or event/stage/seq. relevant defaults. How and where will accumulated defaults be stored and accessed?
        for c in self.immutable: # TODO: immutable holds all cards all the time while other lists gain and lose cards
            c.chosen = 0
            c.holder = None
            c.where = 'deck'
            c.visible = False
        self.buff.clear()
        self.cards.clear()
        # cards is almost always the actionable deck
        self.cards = self.immutable
        self.cards = list(self.cards)

# =============================================================================
#     def __count__(self):
#         return f'{len(self)}'
# =============================================================================

def main():
    """ Describes function main() """
    print('sys.argv[:]:', vars)
    pass

class Jobber():
    """
    Sketch out a framework using serialize module based
    on the need to save jobs and states, evolving via
    abstraction as the project need is identified.
    """
    entries_jobs = {}
    entries_hands = {}
    entries_misc = {}
    memoize = {}

    def __init__(self, jobs=None, hands=None, misc=None):
        """ Serialize some data """
        self.entries_jobs.update(jobs)
        self.entries_hands.update(hands)
#        self.entries_misc.__contains__()
        self.entries_misc.update(misc)

    def _new(self):
        #check exist
        #archive
        pass

    def _modify(self):
        pass

    def _run(self):

        pass

    def mem_o(self, spice):
        # return a token/key for the internally memoized object
        k = str(time.time())
        v = pickle.dumps(spice)
        self.memoize[k] = v
        return k #calling object can keep a keychain, cleanup

    def mem_re(self, k):
        # redeem a token/key for an internally memoized object
        # TODO [] check exists, if not return a default
        v = self.memoize[k]
        spice = pickle.loads(v)
        return spice

    def mem_clean(self, k):
        # object holding key deletes from both the store (here) and self.keychain
        del self.memoize[k]


    @staticmethod
    def jsave(fqfn, obj):
        try:
            return j_son.serial_save(fqfn, obj)
        except:
            print('No JSON save')

    @staticmethod
    def jload(fqfn):
        try:
            return j_son.serial_load(fqfn)
        except:
            print('No JSON load...')

    @staticmethod
    def jclean(fqfn):
        try:
            return j_son.serial_del(fqfn)
        except:
            print('Missing Key, or FileNotFound')

    @staticmethod
    def scavenge():

        jobs = Jobber(jobs={'a':1, 'b':2}, hands={'round':'player', '3D': 'matrix', 'multi-dimensional':3, 'lxml':'associative_array'}, misc={'dunno': 'why'})
    #    print(dir(dir(jobs)))
        print(jobs.entries_jobs.keys())
        print(jobs.entries_hands.keys())
        print(jobs.entries_misc.keys())

    #    Simply does not exist
    #    print('jobs keys()', jobs.keys())
        key = jobs.mem_o(jobs.entries_jobs)
        print(key)
        print('From picked memory,', jobs.mem_re(key))

        # key to loading and unloading external
        jobs.jsave(str(key), jobs.entries_hands)
        try:
            print("""Simple obj saved to local fs in JSON form
                  and retrieved to a variable.""", jobs.jload(str(key))
                  )
        except FileNotFound:
            pass

        try:
            jobs.jclean(str(key))
        except:
            print(r"Can't clean. See exceptions.")


class Actions():

    def __init__():
        pass

    def flip(deck): # proxy for class method Deck.flip_all()
        deck.flip_all()
        deck.restore()

    def pikkle_save(deck=None): #TODO[] too specific pickling the decks... look to deck notes on possible central runtime and offline object lists
        # pickle the instance snapshot
        # TODO add key to keychain, move to Jobber
        for _ in Deck.deckinstances:
            deck = (_[1])
            with open(f'{deck}.pickle', 'wb') as f:
                pickle.dump(deck, f)

    def pikkle_load(deck=None):
        # TODO retrieve key from keychain, move to Jobber
        # load requested
#        if deck:
        # TODO: load all, but only if you haven't deleted them such that they still exist in the list
        for _ in Deck.deckinstances:
            deck = (_[1])
            try: # check if 'a' exists
                print("Deleted a?", a.cards[:]) # TODO replace legacy test (when deleting a)
            except:
                with open(f'{deck}.pickle', 'rb') as f:
                    a = pickle.load(f)

    def j_save(deck):
        pass

    def j_load(deck): # as it is JSON save, JSON load don't handle such complex objects as a Deck instance
        a = deck
        deck = deck.__name__()
        # TODO retreive key from keychain, this calls jload in Jobber
        print(f"{Jobber.jsave(deck, a)}")
        b = Jobber.jload('deck')
        print(b)

    def card_chgproperty_x(deck, card=None, x=None, xval=None):
        # Change a property on all existing Card instances
        if x == None:
            x = 'property_x'
            xval = ['almost_empty']
            for card in deck.immutable:
                for _ in range(len(deck.immutable)):
                    card.chg_property_x(x, xval)
        else: #TODO [] passing a single card with a property and its value via this proxy... hmmmm overdeuplicationalduplicatingnessness get DRY
            pass
        print('Sample from self.immutable:', deck.immutable[0:len(deck.immutable):len(deck.immutable)//20])
        print(f'Class Card says [{Card.makecount}] cards were made.')
    
    def sundry(deck):
        try:
            if len(a.cards[:]) == Card.makecount:
                print('Have a party. The cards are all here.')
        except: # restart the game 'transparently' using other saved data (like what?)
            pass

        # make a deck for each deck
        decks = []
#        for card in deck.immmutable:
#        for deck in deck:
        print('\n\nFirst Round:\n', deck.flip_all())
        print('\n\nSecond Round:\n', deck.flip_all())
        print('\n\nFinal Round:\n', deck.flip_all())
    
    @staticmethod    
    def sortpack_ranks_suits(key):
        return (key['rank'], key['suit'])

class PlayerPools():
    players_list_all = []
    players_online = []
    players_engaged = []
    players_offline = []
    players_error = []


    def __init__(self, game_online, rulepack, deck):
        # TODO [] accommodating currency where there is none at present
        # print(gamename)
        players_playing = []
        if game_online:
            print('The game is online')
        else:
            print('The game status is offline, but we\'re going to play anyway.')
        # TODO list comprehensions here? probably, yes, but loading??? multi-threading???/backgrounding???
        self.players_list_all = self.make_players(rulepack.game['gamename'])
        self.players_online = self.get_players('online', rulepack, deck)
# =============================================================================
#         self.players_engaged = self.get_players('engaged', rulepack, deck)
#         self.players_offline = self.get_players('offline', rulepack, deck)
#         self.players_error = self.get_players('error', rulepack, deck)
# =============================================================================

    def make_players(self, gamename): # TODO [] temporary means of getting players into the system
        fnames = ['John', 'Jake', 'Anna', 'Betty', 'Rowena', 'Garth']
        lnames = ['Ashton', 'Briars', 'Lake', 'Leeds', 'Tunney', 'Sharpe']
#        playersprimer = []
        for fn in fnames:
            for l in lnames:
                PlayerPools.players_list_all.append(Player({
                        'usernum': None,
                        'firstname': fn,
                        'lastname': l,
                        'fullname': f'{fn} {l}',
                        'nickname': f'{fn[0]*2}.{l}',
                        'p_online': True,
                        'p_server': 'Any', # what server is the player connected to
                        'p_connection_quality': 60,
                        # value from 1-100, retrieved from Network/Server, or other
                        # on_server, current, expired, gq_gamename, gq_waittime, gq_abandons
                        'gamehistory': {
                                'gamename':{
                                        'scores':[],
                                        'lastplayed':[],
                                        'skilvl_self': ['beginner', 'intermediate', 'advanced'],
                                        'skilvl_calc': ['calculated numeric weighting within game category']}},
                        'in_game': None} # f'{gamename}' }
                        ))
#        print('List of all players:', PlayerPools.players_list_all)
#        PlayerPools.find_online()

    def get_players(self, seekon, rulepack, deck): # any more than a couple of hundred and this queue is toast
        # check current and waittime, pull from priority queue
# =============================================================================
#         players_list_all = self.players_list_all
#         players_online = self.players_online
#         players_offline = self.players_offline
#         players_engaged = self.players_engaged
#         players_error = self.players_error
# =============================================================================
        players = []

        for _ in PlayerPools.players_list_all: # filter all players to get at online status

            if _.in_game and not _.p_online:
                self.players_error.append(_)

            if _.in_game != None:
                self.players_engaged.append(_)
#                PlayerPools.players_online[_].in_game = True

            if _.p_online and not _.in_game:
                self.players_online.append(_)

#        online = [x for x in PlayerPools.players_list_all if x is x.p_online]
#        print('ONLINE Sample', online[0:-1])
# =============================================================================
#         while len(players) < rulepack.game['player_num'] and seekon == 'online':
#             for _ in self.players_online:
#                 self.players_engaged.append(_)
#                 self.players_engaged[-1].in_game = True
#                 self.players_online.remove(_)
#                 print(_)
#                 players.append(_)
# =============================================================================
#                    self.players_online[_].in_game = False
        while len(players) < rulepack.game['player_num'] and seekon == 'online':
            for _ in range(0, rulepack.game['player_num']):
                p = self.players_online[int(_)]
                self.players_engaged.append(p)
                self.players_engaged[-1].in_game = True
                self.players_online.remove(p)
                players.append((_, p))

#        Thread out a cleanup while the game keeps moving
        # print('doing engaged')
        for _ in self.players_engaged:
            _.in_game = True

        # print('doing error')
        for _ in self.players_error:
            self.players_list_all.remove(_)
            # TODO alert!, send to job... recover valuable card assets... report to insurance company... eek!!!

        # print('doing offline')
        for _ in self.players_offline:
            _.in_game = None # False, True, None (None says not online, False indicates a recent or current lease) TODO sessions and session time stamps

# =============================================================================
#         while len(players) < rulepack.game['player_num'] and seekon == 'offline':
#         while len(players) < rulepack.game['player_num'] and seekon == 'engaged':
# =============================================================================
#
#        online.clear()
#        online.clear()
#        print('PLAYERS in getplayers', players)
# =============================================================================
#         for _ in players:
#             print('Players this hand', _)
        return players
# =============================================================================
### Maybe to divide load or logic later... later
# =============================================================================
#     def find_online(): # TODO [] replication??? terribly, too...
#         for player in PlayerPools.players_list_all:
#             if player.p_online:
#                 if player.ingame == None:
#                     PlayerPools.players_online.append(player)
# =============================================================================

    def set_p_online(self):
        if self.p_online == False:
            self.p_online = True
        else:
            self.p_online = False

    def set_ingame(self):
        if self.ingame == False:
            self.ingame = True
        else:
            self.ingame = False


class Deal():
    pass


class Player():
    """ """
# =============================================================================
#     playerlist = []
#     firstname: str = ''
#     lastname: str = ''
#     fullname: str = f"{firstname} {lastname}"
#     nickname: str = ''
#     p_online: bool = False
#     p_server: str = '' # what server is the player connected to
#     p_connection_quality: int = 60 # value from 1-100, retrieved from Network/Server, or other
#     # on_server, current, expired, gq_gamename, gq_waittime, gq_abandons
#
#     gamehistory = {'gamename':
#         {'scores':[],
#          'lastplayed':[],
#          'skilvl_self': ['beginner', 'intermediate', 'advanced'],
#          'skilvl_calc': ['\# calculated numeric weighting within game category'],
#          '...':00000}}
# =============================================================================
    usernum = (f'{x}' for x in range(10000, 1000000, 50))

    def __init__(self, dictionary):
        self.__dict__.update(dictionary)
        self.usernum = Player.usernum.__next__()
# =============================================================================
#         self.fullname = dictionary['fullname']
#         self.nickname = dictionary['nickname']
#         self.p_online = dictionary['p_online']
#         self.ingame = dictionary['ingame']
#         self.gamename = dictionary['gamehistory']['gamename']
# =============================================================================
# =============================================================================
#         print(self.fullname)
#         print(self.nickname)
#         print(self.p_online)
# =============================================================================
        """ players known to the player server or to the stand-alone game engine, or both """
        """ initialize a list of players from external input, faking a client/server """
#        print(dictionary)

    def __repr__(self):
        """ """
        return f'[usernum="{self.usernum}", fullname="{self.fullname}", nickname="{self.nickname}", p_online="{self.p_online}, p_server="{self.p_server}", in_game="{self.in_game}"]\n'

    def on_player():
        # check the server for the user, if there set on_server True, else False
        if server.player == True: #TODO [] sketches being sketcy, waits on real server
            self.on_server = True


class Server():
    on_server = None

    def __init__(self):
        on_server = players.on_server


class Stack():

    def __init__(self):
        self.list = []

    def __len__(self):
        return len(self.list)

    def push(self, item):
            self.list.append(item)

    def pop(self, item=None): # default is last, item is there for convenience
        return self.list.pop()

class Hand():
    """
    # has
    player from Player
    card from Card
    round from Rulepack or Rulepack::Sequence
    # calls / changes
    Game status
    Card value
    Player score
    Partner score
    Win/Lose status
    """
    players = []
#     round = {}
    # {r1: cards[(card, player[i]), (card, player[i])], r2: cards[(card, player[i]), (card, player[i])]...}

    def __init__(self, rulepack, deck, players):
        # must initiate play using the state machine
        self.round = {} #Hand.round
#         print('players in hand - har',players)
        self.make_hand(rulepack, deck, players, self.round)

    def make_hand(self, rulepack, deck, players, round):
        # assign player(s) to card in turn
        # TODO [] alternate the deal between players
#        players = Hand.players = Hand.getplayers(rulepack, deck)
#        players = self.players
        # TODO [] does not presently provide for a review of cards, so an object token might fit to an array of arrays with holder at the top
        # maybe saving each round's cards... what's common among games?
#         print('top of make hand', players)
        player_num = rulepack.game['player_num']
        for _ in range(0, rulepack.game['cards_ea_rnd']):
            roundkey = f'r{_}'
            dealt = []
            for playing in players:
                keypress = False
                while keypress is False:
#                     print("deck.cards.length()", len(deck.cards))
                    pick = choice(deck.cards)
                    dealt.append((pick.rank, pick.suit, playing[1].usernum))
                    self.round[roundkey] = dealt
                    deck.cards.remove(pick)
                    deck.buff.append(pick)
                    Card.switchhold(pick, playing)
#                     print(f'{playing[1].fullname} receives the {pick.name}')
                    msg = '\t' + f'// {playing[1].fullname}' +  '  ===>\t' + f' / {pick.name} //  |->' 
                    keypress = input(msg)
#                     print("deck.immutable.length()", len(deck.immutable))
#                     print('deck.buff.length()', len(deck.buff))
            self.calc_round(self.round, roundkey)
#             print(self.round)
#         print('bottom of make hand', players)
        self.calc_hand(rulepack, deck, players, self.round)

        # pick the dealer
        # pick the direction of the deal
        # pick the first player to receive a card
        # pick the starting player

    def calc_round(self, round, roundkey):
#        print('Recalculate cards as per context of round.')
        # count rounds calculated
        header = """
    Calculating Rounds
    =================="""[1:]
        print('\n' + f'{header}')
        print('This ::\r' + f'  {roundkey} --> {round[roundkey]}')
        print('To date ::')
        for k, v in round.items():
            print(f' {k, v}')
        print('\n')

#        for k in round.keys():
#            # set a counted property on a counted round
#            #
#            print(k)
        # on final round, pitch to calchand in the calling makehand()?
#         # sort the cards and then assign a winner according to rules // end of all rounds
        res = sorted(round[roundkey], key=lambda rank: str(rank[0]), reverse=True)
        print('Sorted by rank as a string. Still need to set an intinsic value and set up some kind of value setting matrix or figure out where it fits re.rules/calculating hands, creating gameplay, augmenting or deprecating the //beats// system',res)

    def calc_hand(self, rulepack, deck, players, round): #calculate the hand using values in immutable
        """ been moving cards from one array to another, but setting values may be easier
        what's the utility in having a deck whose cards are caught between arrays???
        """  

        allheld = [] # temporary card search results buffer
        for player in players:
            for card in deck.immutable: # buff is the medium #TODO check
                if card.holder == player[1]:
                    allheld.append(card)
#            print('Len AllHeld', len(allheld))
#            print('Len BUFF', len(deck.buff))
        if len(allheld) == len(deck.buff): # no trailing cards?? #TODO make more accurate check ie. len and content
            print('CalcHand Cards HELD by players this hand', allheld)
            allheld.clear()
        else:
            f'dynamax is neato' #TODO build a geodesic dome... actually, I would prefer an aircrete dome
        header = """
    Calculating Game Results
    ========================"""[1:]
        print('\n' + f'{header}')            
        print('In-Game Status Updates: Winner(s), Loser(s), Updates(interface, tableset, as necessary) for \n', players) #TODO
        for k, v in round.items():
            # TODO parse the dict extracting what's necessary to validate immediate results
            # TODO then send it to a server side job for player/game history statistical processing
            # TODO along with the results from the hand and game results
            # such processing will have to take into account the conditions of the game
            # and changes within it, such as player abandons followed by in game player replacements
            # network speed and game speed conditions which variate inside and outside norms... norms being the sought after results of calculation
             print(f'{k, v}')
             
# =============================================================================
#         getsorted = [] # TODO 
#         # sort the cards and then assign a winner according to rules // end of all rounds
#         getsorted = sorted(round['r0'], key=lambda rank, rank[0])
#         print(getsorted)
# =============================================================================
        
        print('Sending each round\'s results to server') # TODO
        del round
        
        print('Sending Game results to server.') # TODO
        del allheld
        
        print('Selecting waiting players to join game.') # TODO
        del players

#        deck.restore()
        app() # TODO [xxx] play endlessly and without a care in the world


class Rulepack(): # Games
    """ """
    #    game = {}
    
    def __init__(self, game):
        # append the dictionary of current game rules accessible as rulepack.game['key']
#        print(game)
        game = self.gamepick(game)
        self.game = {}
        self.game.update(game)
        self.game['intrinsic'] = True
        self.game['intrinsic'] = True
        self.game['intrinsic'] = True

    def gamepick(self, game):
        # TODO [] from external file {'games': {'gamename': 'gameX', }}
        # TODO [] redesign initial card data to reflect absolute value of suit cards and any value factors on suits, ranks, etc
        return {'gamename': 'game_x',
                'cards_ea_rnd': 5,
                'player_num': 4,
                'numdecks': 10,
                'suits': ['Men', 'Hobbits', 'Elves', 'Orcs'],
                'ranks': [2, 3, 4, 5, 6, 7, 8, 9, 10, 100],
                'bosses': ['Trailer Park Super', 'Jack', 'Queen', 'King', 'Ace']
                }
        return {'gamename': 'game_x',
                'cards_ea_rnd': 5,
                'player_num': 4,
                'numdecks': 10,
                'suits': ['Men', 'Hobbits', 'Elves', 'Orcs'],
                'ranks': [2, 3, 4, 5, 6, 7, 8, 9, 10, 100],
                'bosses': ['Trailer Park Super', 'Jack', 'Queen', 'King', 'Ace']
                }


class Sequencer(Rulepack): # TODO blend of Observer and State Machine pattern functionality, ZMQ candidate?
    """ refines rounds and implement or calls on timer(s) """
    '''
#     TODO 
    The investigation into using class Stack() pattern as an object repository
    resulted in identifying the need to also handle events. Also, as a lookahead to
    internetworking, and perhaps also to turning toward a microservice architecture with minimal 
    work involved in carving the functionality out of the structure, the desire to accommodate
    threads and pipes.
    
    Serialization... XML and JSON with renamed Pickle objects
    Data loss, Data valuation schemes in a schema
    Morpology through XML-led JSON object exchange evolution
    
    '''
    pass

class Display(): # has DisplayElements()
    """ rendering order of DisplayElement objects """
    pass

class DisplayElements():
    """
    # for all players
    set_board = 0
    # for each player
    set_seat = 0 # 0 thru number of seats, incl. observer
    set_hand = [] # is hand according to Player and Dealer
    set_cards = {'orientation': ['fan', 'loose']]
    set_status = {'suit': '', 'isdealer': False, 'etc': 999}
    # for all players
    set_rub = 0
    set_pot = {} # car
    pop_status = 0
    pop_score = 0
    pop_selection = 0
    # actions for Hand
        # evaluate hand
        # set win/lose status
        # reset card owner, round value
    """
    pass

def app():
    
#    txt = urwid.Text(u"Hello World")
#    fill = urwid.Filler(txt, 'top')
#    loop = urwid.MainLoop(fill)
#    loop.run()
    """ Well, not really an app, more of a test bed."""
    stack = Stack()
    stack.push('''66_|--|_||_99''')

    print('\nFrom the stack to you ::  ', stack.list,'  ;)')
    # decide to play a game and call its rules up
    rulepack = Rulepack('biggame')
    #    print(rulepack.game)
    # Enter players onto the system and assign to game if status is online
    # Instance cards from Deck()
    deck = Deck(rulepack) # could make it more efficient and simply add count to each replica, counting down as one is used.
    game_online = False

    # TODO [] from list of players interested in playing gameX
    players = PlayerPools(game_online, rulepack, deck)
    players_online = players.players_online
    
# =============================================================================
# #    deck.buff.clear() # TODO [] May have to restructure class Card() and 
#      #Deck.<instance>.cardlists, possibly putting the lists on the class...
#     deck.buff = deck.immutable[:]
# #    print(sorted(deck.buff, key=lambda buff: str(buff.rank)))
#     print(sorted(deck.buff, key=Actions.sortpack_ranks_suits, reverse=True))
# =============================================================================

#    print(deck.buff)
    
    #TODO players.players_engaged, players_online, players_offline, Generally PlayerPool access regimes/ events, etc.
#     print('Meet the table: \n', players.players_engaged)
    print('Number of players on system:\t Too early to say...')
    print('Player profiles with errors:', len(players.players_error))
    print('Number of players offline:', len(players.players_offline))
    print('Number of players online:', len(PlayerPools.players_list_all)) # TODO try len(players.players_list_all) -- does this show a problem with PlayerPools and data access upon instance creation
    print('Meet the table: \n', players.players_online)



#    print(f'Class Card says [{Card.makecount}] cards were made.')
#    print(len(deck.cards))
    play = Hand(rulepack, deck, players_online)
    # invocation could change if the game dict is put on Rulepack() instance at construction/selection
#    play = Hand(rulepack(game), deck)
    print('Deck Instances', Deck.deckinstances)
#    Actions.pikkle_save()
#    Actions.flip(a)
#    Actions.card_chgproperty_x(deck)


if __name__ == "__main__":

#    vars=[]
#    vars = sys.argv[:]
#    main()
    app()
#    print(Player.__dict__)
# else:
#     print('Not happening...')


